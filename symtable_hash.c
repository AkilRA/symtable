#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>
#include"hash.h"
#include"symtable.h"

const static long int maxSymTableSize[8] = { 509, 1021, 2039, 4093, 8191, 16381, 32749, 65521 };

//Node
struct Node
{
    const char *key;
    const void *value;
    struct Node *next;
};

//Symbol Table
struct SymTable
{
    struct Node **node;
    int len, index;
};
typedef struct SymTable *SymTable_t;


//Creates a new SymTable structure with no bindings
SymTable_t SymTable_new(void)
{
    SymTable_t oSymTable = malloc(sizeof(struct SymTable));
    if (oSymTable == NULL)
        return NULL;

    oSymTable->node = malloc(sizeof(struct Node*) * maxSymTableSize[0]);
    for (int i = 0; i < maxSymTableSize[0]; i++)
        oSymTable->node[i] = NULL;
    oSymTable->len = 0;
    oSymTable->index = 0;
    return oSymTable;
}

//Frees all the memory occupied by the symbol table
void SymTable_free(SymTable_t oSymTable)
{
    if (oSymTable == NULL)
        return;
    for (int i = 0; i < maxSymTableSize[oSymTable->index]; i++)
    {
        struct Node *temp1, *temp = oSymTable->node[i];
        while (temp != NULL)
        {
            free((char*)temp->key);
            temp->value = NULL;
            temp1 = temp;
            temp = temp->next;
            free(temp1);
        }
    }
    free(oSymTable->node);
    free(oSymTable);
}

//Returns the total number of bindings
int SymTable_getLength(SymTable_t oSymTable)
{
    if (oSymTable == NULL)
        return -1;
    else
        return oSymTable->len;
}

//To insert a binding into the symbol table
int SymTable_put(SymTable_t oSymTable, const char *pcKey, const void *pvValue)
{
    for (int i = 0; i < maxSymTableSize[oSymTable->index]; i++)
    {
        struct Node *temp = oSymTable->node[i];
        assert(pcKey != NULL);
        assert(pvValue != NULL);
        //Check if pcKey binding is already in the symbol table
        while (temp != NULL)
        {
            //If pcKey is a already in the SymTable
            if (strcmp(temp->key, pcKey) == 0)
                return 0;
            temp = temp->next;
        }
    }

    oSymTable->len++;

    if (oSymTable->len > maxSymTableSize[oSymTable->index] && (oSymTable->index != 7) )
    {
        SymTable_t newSymTable = rehash(oSymTable->index);
        for (int i = 0; i < maxSymTableSize[oSymTable->index]; i++)
        {
            struct Node *temp = oSymTable->node[i];
            while (temp != NULL)
            {
                SymTable_put(newSymTable, temp->key, temp->value);
                temp = temp->next;
            }
        }
        SymTable_put(newSymTable, pcKey, pvValue);
        struct Node **swap = oSymTable->node;
        oSymTable->node = newSymTable->node;
        newSymTable->node = swap;
        (newSymTable->index)--;
        SymTable_free(newSymTable);
        (oSymTable->index)++;
        return 1;
    }

    //Create a new binding with pcKey and pvValue
    struct Node *newnode = malloc(sizeof(struct Node));
    //Check if no memory is available
    if (newnode == NULL)
        return 0;

    newnode->key = malloc(sizeof(char) * (strlen(pcKey) + 1));
    strcpy((char *)newnode->key, pcKey);
    newnode->value = pvValue;
    newnode->next = NULL;

    int hashval;
    hashval = SymTable_hash(pcKey, maxSymTableSize[oSymTable->index]);
    struct Node *temp = oSymTable->node[hashval];
    if (temp == NULL)
    {
        oSymTable->node[hashval] = newnode;
    }
    else
    {
        while (temp->next != NULL)
            temp = temp->next;
        temp->next = newnode;
    }
    return 1;
}

//To check whether the binding is present in the symbol table
void *SymTable_get(SymTable_t oSymTable, const char *pcKey)
{
    int hashval;
    struct Node *temp = NULL;
    hashval = SymTable_hash(pcKey, maxSymTableSize[oSymTable->index]);
    temp = oSymTable->node[hashval];
    assert(pcKey != NULL);
    while (temp != NULL)
    {
        //Check to locate the binding with pcKey
        if (strcmp(temp->key, pcKey) == 0)
            return (char *)temp->value;
        temp = temp->next;
    }
    return NULL;
}


//To get the binding present in the symbol table
int SymTable_contains(SymTable_t oSymTable, const char *pcKey)
{
    int hashval;
    struct Node *temp = NULL;
    hashval = SymTable_hash(pcKey, maxSymTableSize[oSymTable->index]);
    temp = oSymTable->node[hashval];
    assert(pcKey != NULL);
    while (temp != NULL)
    {
        //Check to locate the binding with pcKey
        if (strcmp(temp->key, pcKey) == 0)
            return 1;
        temp = temp->next;
    }
    return 0;
}



//To remove a binding from the symbol table
void *SymTable_remove(SymTable_t oSymTable, const char *pcKey)
{
    int hashval;
    char* str;
    struct Node *temp, *temp1 = NULL;
    assert(pcKey != NULL);
    hashval = SymTable_hash(pcKey, maxSymTableSize[oSymTable->index]);
    temp = oSymTable->node[hashval];
    if (temp == NULL)
        return NULL;

    if (strcmp(temp->key, pcKey) == 0)
    {
        temp1 = temp;
        oSymTable->node[hashval] = temp->next;
        temp1->next = NULL;
        str = (char*)temp1->value;
        free((char*)temp1->key);
        free(temp1);
        return str;
    }
    while (temp->next != NULL)
    {
        if (strcmp(temp->next->key, pcKey) == 0)
        {
            temp1 = temp->next;
            temp->next = temp->next->next;
            temp1->next = NULL;
            str = (char*)temp1->value;
            free((char*)temp1->key);
            free(temp1);
            return str;
        }
        temp = temp->next;
    }
    return NULL;
}

//To replace a binding from the symbol table
void *SymTable_replace(SymTable_t oSymTable, const char *pcKey, const void *pvValue)
{
    int hashval;
    char *str;
    struct Node *temp1, *n, *temp = NULL;
    assert(pcKey != NULL);
    assert(pvValue != NULL);
    hashval = SymTable_hash(pcKey, maxSymTableSize[oSymTable->index]);
    temp = oSymTable->node[hashval];
    n = malloc(sizeof(struct Node));
    while (temp != NULL)
    {
        //Check to locate the binding with pcKey
        if (strcmp(temp->key, pcKey) == 0)
        {
            n->key = malloc(sizeof(char)* (strlen(pcKey) + 1));
            strcpy((char *)n->key, temp->key);
            n->value = temp->value;
            temp1 = n;
            temp->value = pvValue;
            str = (char*)temp1->value;
            free((char*)n->key);
            free(n);
            return str;
        }
        temp = temp->next;
    }
    free(n);
    return NULL;
}

//To implement MAP using a function pointer
void SymTable_map(SymTable_t oSymTable, void(*pfApply) (const char *pcKey, const void *pvValue, void *pvExtra), const void *pvExtra)
{
    if (oSymTable == NULL)
        return;
    printf("\nMap Test - Printing Entries.\n");
    for (int i = 0; i < maxSymTableSize[oSymTable->index]; i++)
    {
        struct Node *temp = oSymTable->node[i];
        while (temp != NULL)
        {
            (*pfApply)(temp->key, temp->value, (char*)pvExtra); //calls printBinding function
            temp = temp->next;
        }
    }
}

//To increase the bucket size if symbol table is full
static SymTable_t rehash(long int size)
{
    SymTable_t newSymTable = malloc(sizeof(struct SymTable));
    if (newSymTable == NULL)
        return NULL;

    newSymTable->node = malloc(sizeof(struct Node*) * maxSymTableSize[size + 1]);
    for (int i = 0; i < maxSymTableSize[size + 1]; i++)
        newSymTable->node[i] = NULL;
    newSymTable->len = 0;
    newSymTable->index = size + 1;
    return newSymTable;
}