#include<stdio.h>
#include<conio.h>
#include<assert.h>
#include<string.h>
#include<stdlib.h>
#include"symtable.h"
#define M 100000
#define N 6

typedef struct SymTable *SymTable_t;

void printBinding(const char *key, const void *value, void *ptr){
    printf("Key: %s, Value: %s\n", key, (char*)value);
}

//Function to test symbol table structure creation
void test_SymTable_new()
{
    int count;
    SymTable_t oSymTable = SymTable_new();
    assert(oSymTable != NULL);
    count = SymTable_getLength(oSymTable);
    assert(count == 0);
    SymTable_free(oSymTable);
}

//Function to test symbol table freeing
void test_SymTable_free()
{
    int success;
    SymTable_t oSymTable = SymTable_new();
    SymTable_t noSymTable = NULL;

    //To push 5 keys and values
    success = SymTable_put(oSymTable, "ab", "int");
    success = SymTable_put(oSymTable, "b", "int");
    success = SymTable_put(oSymTable, "c", "float");
    success = SymTable_put(oSymTable, "d", "char");

    //Clears the symbol table
    SymTable_free(oSymTable);
    //When no symbol table is passed
    SymTable_free(noSymTable);
    assert(noSymTable == NULL);
}


//Function to test the get length function
void test_SymTable_getLength()
{
    int count, success;
    SymTable_t oSymTable = SymTable_new();
    //Gets the number of bindings in the symbol table
    count = SymTable_getLength(oSymTable);
    assert(count == 0); //Initially no elements are pushed

    //To push 5 keys and values
    success = SymTable_put(oSymTable, "ab", "int");
    success = SymTable_put(oSymTable, "b", "int");
    success = SymTable_put(oSymTable, "c", "float");
    success = SymTable_put(oSymTable, "d", "char");

    //4 elements are pushed
    count = SymTable_getLength(oSymTable);
    assert(count == 4);

    //Increase bucket size
    success = SymTable_put(oSymTable, "e", "char");
    success = SymTable_put(oSymTable, "f", "char");
    success = SymTable_put(oSymTable, "g", "char");
    success = SymTable_put(oSymTable, "h", "char");


    //8 elements are pushed
    count = SymTable_getLength(oSymTable);
    assert(count == 8);


    //When no symbol table is passed
    count = SymTable_getLength(NULL);
    assert(count == -1);

    SymTable_free(oSymTable);
}


//Function to test the search function that returns 1 or 0
void test_SymTable_contains()
{
    int success;
    SymTable_t oSymTable = SymTable_new();

    //To push 5 keys and values
    success = SymTable_put(oSymTable, "ab", "int");
    success = SymTable_put(oSymTable, "b", "int");
    success = SymTable_put(oSymTable, "c", "float");
    success = SymTable_put(oSymTable, "d", "char");

    //To check whether the binding is present
    success = SymTable_contains(oSymTable, "ab");
    assert(success == 1);

    success = SymTable_contains(oSymTable, "b");
    assert(success == 1);

    success = SymTable_contains(oSymTable, "c");
    assert(success == 1);

    success = SymTable_contains(oSymTable, "gh"); //No key 'gh'
    assert(success == 0); //Returns 0

    SymTable_free(oSymTable);
}


//Function to test the creation of binding with keys and values
void test_SymTable_put()
{
    int success;
    SymTable_t oSymTable = SymTable_new();
    success = SymTable_put(oSymTable, "a", "int");
    assert(success == 1);

    success = SymTable_put(oSymTable, "b", "int");
    assert(success == 1);

    success = SymTable_put(oSymTable, "c", "float");
    assert(success == 1);

    success = SymTable_put(oSymTable, "c", "char"); //Same key pushed
    assert(success == 0);//Returns 0

    //Increase bucket size
    success = SymTable_put(oSymTable, "d", "float");
    success = SymTable_put(oSymTable, "e", "char");
    success = SymTable_put(oSymTable, "f", "char");
    success = SymTable_put(oSymTable, "g", "char");
    success = SymTable_put(oSymTable, "h", "char");
    success = SymTable_put(oSymTable, "i", "float");
    success = SymTable_put(oSymTable, "j", "float");
    success = SymTable_put(oSymTable, "k", "float");
    success = SymTable_put(oSymTable, "l", "float");
    success = SymTable_put(oSymTable, "m", "float");
    success = SymTable_put(oSymTable, "n", "float");
    success = SymTable_put(oSymTable, "o", "float");
    success = SymTable_put(oSymTable, "p", "float");
    success = SymTable_put(oSymTable, "q", "float");
    success = SymTable_put(oSymTable, "r", "float");
    success = SymTable_put(oSymTable, "s", "float");
    success = SymTable_put(oSymTable, "t", "float");
    success = SymTable_put(oSymTable, "u", "float");
    success = SymTable_put(oSymTable, "v", "float");
    success = SymTable_put(oSymTable, "w", "float");
    success = SymTable_put(oSymTable, "x", "float");
    success = SymTable_put(oSymTable, "y", "float");
    success = SymTable_put(oSymTable, "z", "float");
    SymTable_free(oSymTable);
}


//Function to test the search function that returns binding value or NULL
void test_SymTable_get()
{
    int success;
    void* pvValue;
    SymTable_t oSymTable = SymTable_new();

    //To push 8 keys and values
    success = SymTable_put(oSymTable, "e", "char");
    success = SymTable_put(oSymTable, "f", "char");
    success = SymTable_put(oSymTable, "g", "char");
    success = SymTable_put(oSymTable, "h", "char");
    success = SymTable_put(oSymTable, "ab", "int");
    success = SymTable_put(oSymTable, "b", "int");
    success = SymTable_put(oSymTable, "c", "float");
    success = SymTable_put(oSymTable, "d", "char");

    //To check whether the binding is present
    pvValue = SymTable_get(oSymTable, "c");
    assert(pvValue != NULL);

    pvValue = SymTable_get(oSymTable, "ab");
    assert(pvValue != NULL);

    pvValue = SymTable_get(oSymTable, "b");
    assert(pvValue != NULL);

    pvValue = SymTable_get(oSymTable, "e");
    assert(pvValue != NULL);

    pvValue = SymTable_get(oSymTable, "gh"); //No key 'gh'
    assert(pvValue == NULL); //No value

    SymTable_free(oSymTable);
}

void test_SymTable_remove()
{
    int success;
    void* pvValue;
    SymTable_t oSymTable = SymTable_new();

    //To push 5 keys and values
    success = SymTable_put(oSymTable, "ab", "int");
    success = SymTable_put(oSymTable, "b", "int");
    success = SymTable_put(oSymTable, "c", "float");
    success = SymTable_put(oSymTable, "d", "char");
    success = SymTable_put(oSymTable, "e", "double");

    //Remove Key ab
    pvValue = SymTable_remove(oSymTable, "ab");
    assert(strcmp(pvValue, "int") == 0);

    //Remove Key c
    pvValue = SymTable_remove(oSymTable, "c");
    assert(strcmp(pvValue, "float") == 0);

    //Remove last element added
    pvValue = SymTable_remove(oSymTable, "e");
    assert(strcmp(pvValue, "double") == 0);

    //Element not found since it is removed before
    pvValue = SymTable_remove(oSymTable, "e");
    assert(pvValue == NULL);

    SymTable_free(oSymTable);
}

void test_SymTable_replace()
{
    int success;
    void* pvValue;
    SymTable_t oSymTable = SymTable_new();

    //To push 5 keys and values
    success = SymTable_put(oSymTable, "ab", "int");
    success = SymTable_put(oSymTable, "b", "int");
    success = SymTable_put(oSymTable, "c", "float");
    success = SymTable_put(oSymTable, "d", "char");
    success = SymTable_put(oSymTable, "e", "double");

    //Replace int by INT
    pvValue = SymTable_replace(oSymTable, "ab", "INT");
    assert(strcmp(pvValue, "int") == 0);

    //Replace float by FLOAT
    pvValue = SymTable_replace(oSymTable, "c", "FLOAT");
    assert(strcmp(pvValue, "float") == 0);

    //Replace double by DOUBLE
    pvValue = SymTable_replace(oSymTable, "e", "DOUBLE"); //double to DOUBLE
    assert(strcmp(pvValue, "double") == 0);

    //Replace double by DOUBLE
    pvValue = SymTable_replace(oSymTable, "e", "double"); // DOUBLE to double
    assert(strcmp(pvValue, "DOUBLE") == 0);

    //Element not found
    pvValue = SymTable_replace(oSymTable, "g", "DOUBLE"); //No key 'g'
    assert(pvValue == NULL); //No value

    SymTable_free(oSymTable);
}


void test_SymTable_map()
{
    int success;
    SymTable_t noSymTable = NULL, oSymTable = SymTable_new();

    //To push 5 keys and values
    success = SymTable_put(oSymTable, "a", "int");
    success = SymTable_put(oSymTable, "b", "int");
    success = SymTable_put(oSymTable, "c", "float");
    success = SymTable_put(oSymTable, "d", "char");
    success = SymTable_put(oSymTable, "e", "double");

    //Prints all the bindings
    SymTable_map(oSymTable, &printBinding, "Akil"); //'Akil' as pvExtra
    assert(oSymTable != NULL);
    //Prints all the bindings
    SymTable_map(oSymTable, &printBinding, NULL); //'NULL' as pvExtra
    assert(oSymTable != NULL);
    //No symbol table
    SymTable_map(noSymTable, &printBinding, NULL);
    assert(noSymTable == NULL);

    SymTable_free(oSymTable);
}

void finalTest()
{
    int success = 0, count = 0;
    int A = 65, Z = 90, a = 97, b = 122;
    char **key, **value;

    key = (char **)malloc(sizeof(char*)*M);
    value = (char **)malloc(sizeof(char*)*M);
	for (int i = 0; i < M; i++)
	{
		key[i] = (char *)malloc(sizeof(char)*N);
		value[i] = (char *)malloc(sizeof(char)*N);
	}


    //To generate 100000 entries
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < N - 1; j++)
        {
            key[i][j] = (rand() % (Z - A + 1)) + Z;
            key[i][5] = '\0';
            value[i][j] = (rand() % (b - a + 1)) + b;
            value[i][5] = '\0';
        }
    }

    //All tests
    printf("\nFINAL TEST WITH 100,000 ENTRIES:");
    SymTable_t oSymTable = SymTable_new();
    printf("\nSymTable_new()          [PASSED]");

    for (int i = 0; i < M; i++)
    {
        success = SymTable_put(oSymTable, key[i], value[i]);
        if (success)
            count++;
    }
    printf("\nSymTable_put()          [PASSED]");

    assert(count == SymTable_getLength(oSymTable));
    printf("\nSymTable_getLength()    [PASSED]");

    for (int i = 0; i < M; i++)
        SymTable_replace(oSymTable, key[i], "int");
    printf("\nSymTable_replace()      [PASSED]");

    for (int i = 0; i < M; i++)
        SymTable_get(oSymTable, key[i]);
    printf("\nSymTable_get()          [PASSED]");

    for (int i = 0; i < M; i++)
        success = SymTable_contains(oSymTable, key[i]);
    printf("\nSymTable_contains()     [PASSED]");

    for (int i = 0; i < M; i++)
        SymTable_remove(oSymTable, key[i]);
    printf("\nSymTable_remove()       [PASSED]");

    SymTable_free(oSymTable);
    printf("\nSymTable_free()         [PASSED]");
    printf("\n\n[100%% SUCCESSFULLY PASSED]");

    for (int i = 0; i < M; i++)
    {
        free(key[i]);
        free(value[i]);
    }
    free(key);
    free(value);
}


int main()
{
    test_SymTable_new();
    test_SymTable_put();
    test_SymTable_getLength();
    test_SymTable_contains();
    test_SymTable_get();
    test_SymTable_free();
    test_SymTable_map();
    test_SymTable_replace();
    test_SymTable_remove();
    finalTest();
    _getch();
    return 0;
}