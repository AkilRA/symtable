# Symbol Table Program in C   31/09/2018
**************************************

Developer: Akil Ayyappan R

Email: akil@multicorewareinc.com


# INDEX
- 1)Introduction
- 2)Files
- 3)Functions
- 4)Makefiles Commands

# 1.INTRODUCTION
Symbol Table is an important data structure created and maintained by the compiler in order to keep track 
of semantics of variable i.e.it stores information about scope and binding information about names, information about 
instances of various entities such as variable and function names, classes, objects, etc.

Symtable can be implemented in 4 approaches 1) Linked List 2) Hash 3) List and 4) Binary Search Tree.

The two implementations done are
### Linked List –
* This implementation is using linked list. A link field is added to each record.
* Searching of names is done in order pointed by link of link field.
* A pointer “Firstnode” is maintained to point to first record of symbol table.
* Insertion is fast O(1), but lookup is slow for large tables – O(n) on average

### Hash Table –
* In hashing scheme two tables are maintained – a hash table and symbol table and is the most commonly used method to implement symbol tables..
* A hash table is an array with index range: 0 to tablesize – 1.These entries are pointer pointing to names of symbol table.
* To search for a name we use hash function that will result in any integer between 0 to tablesize – 1.
* Insertion and lookup can be made very fast – O(1).
* Advantage is quick search is possible and disadvantage is that hashing is complicated to implement.

# 2.FILES
	hash.h : Hash function declaration.
	symtable.h : Structure and Function declarations .
	hash.c : Hash function definition.
	symtable_link.c : Function definitions for Linked list implementation.
	symtable_hash.c : Function definitions for Hash implementation.
	symtable_test.c : Test functions and function calls.
	makefile : Compile and run files.

# 3.FUNCTIONS
- [SymTable_t SymTable_new(void)] : Create new symbol table.
- [void SymTable_free(SymTable_t oSymTable)] : Delete symbol table.
- [int SymTable_getLength(SymTable_t oSymTable)] : Number of elements in symbol table.
- [int SymTable_put(SymTable_t oSymTable,const char* pcKey,const void* pvValue)] : Insert keys and values into symbol table.
- [void* SymTable_replace(SymTable_t oSymTable, const char* pcKey, const void* pvValue)] : Replace specific binding in symbol table.
- [int SymTable_contains(SymTable_t oSymTable, const char* pcKey)] : Check if Key is present in symbol table.
- [void* SymTable_get(SymTable_t oSymTable, const char* pcKey)] : Get specific key binding in symbol table.
- [void* SymTable_remove(SymTable_t oSymTable, const char *pcKey)] : Remove binding from symbol table.
- [void SymTable_map(SymTable_t oSymTable,void (*pfApply)(const char* pcKey,const void* pcValue,void* pvExtra), const void* pvExtra)]  : Pass all bindings into given function.
- [void printSymTable(SymTable_t oSymTable)] : Print all bindings in Symbol table.

# 4.MAKE COMMANDS
- make all : Build all files.
- make test: Run linked list and hash exe.
- make hashexe : Build and run hash exe.
- make linkexe : Build and run linked exe.
- make clean : Clean all exe files.
