
all:
	gcc symtable_test.c symtable_link.c symtable.h -o test_linked.exe
	gcc symtable_test.c symtable_hash.c symtable.h hash.c hash.h -o test_hash.exe

test:
	test_linked.exe
	test_hash.exe
	
clean:
	del /f test_linked.exe test_hash.exe