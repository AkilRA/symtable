#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>
#include"symtable.h"

//Node
struct Node
{
    const char *key;
    const void *value;
    struct Node *next;
};


//Symbol Table
struct SymTable
{
    struct Node *firstnode;
    int len;
};
typedef struct SymTable *SymTable_t;

//Creates a new SymTable structure with no bindings
SymTable_t SymTable_new(void)
{
    SymTable_t oSymTable = malloc(sizeof(struct SymTable));
    if (oSymTable == NULL)
        return NULL;
    oSymTable->firstnode = NULL;
    oSymTable->len = 0;
    return oSymTable;
}

//Frees all the memory occupied by the symbol table
void SymTable_free(SymTable_t oSymTable)
{
    if (oSymTable == NULL)
        return;
    struct Node *temp1, *temp = oSymTable->firstnode;
    while (temp != NULL)
    {
        free((char*)temp->key);
        temp->value = NULL;
        temp1 = temp;
        temp = temp->next;
        free(temp1);
    }
    free(oSymTable);
}

//Returns the total number of bindings
int SymTable_getLength(SymTable_t oSymTable)
{
    if (oSymTable == NULL)
        return -1;
    else
        return oSymTable->len;
}

//To get the binding present in the symbol table
int SymTable_contains(SymTable_t oSymTable, const char *pcKey)
{
    struct Node *temp = oSymTable->firstnode;
    //Check if the SymTable is empty
    if (temp == NULL)
        return 0;
    assert(pcKey != NULL);
    while (temp != NULL)
    {
        //Check to locate the binding with pcKey
        if (strcmp(temp->key, pcKey) == 0)
        {
            return 1;
        }
        temp = temp->next;
    }
    return 0;
}

//To insert a binding into the symbol table
int SymTable_put(SymTable_t oSymTable, const char *pcKey, const void *pvValue)
{
    struct Node *temp = oSymTable->firstnode;
    assert(pcKey != NULL);
    assert(pvValue != NULL);
    //Check if pcKey binding is already in the symbol table
    while (temp != NULL)
    {
        //If pcKey is a already in the SymTable
        if (strcmp(temp->key, pcKey) == 0)
            return 0;
        temp = temp->next;
    }

    //Create a new binding with pcKey and pvValue
    struct Node *newnode = malloc(sizeof(struct Node));
    //Check if no memory is available
    if (newnode == NULL)
        return 0;

    newnode->key = malloc(sizeof(char) * (strlen(pcKey) + 1));
    strcpy((char *)newnode->key, pcKey);
    newnode->value = pvValue;
    newnode->next = NULL;

    struct Node *head = oSymTable->firstnode;
    if (head == NULL)
    {
        head = newnode;
        oSymTable->firstnode = head;
    }
    else
    {
        while (head->next != NULL)
        {
            head = head->next;
        }
        head->next = newnode;
    }
    oSymTable->len++;
    return 1;
}

//To check whether the binding is present in the symbol table
void *SymTable_get(SymTable_t oSymTable, const char *pcKey)
{
    struct Node *temp = oSymTable->firstnode;
    //Check if the SymTable is empty
    if (temp == NULL)
        return 0;
    assert(pcKey != NULL);
    while (temp != NULL)
    {
        //Check to locate the binding with pcKey
        if (strcmp(temp->key, pcKey) == 0)
            return (char *)temp->value;
        temp = temp->next;
    }
    return NULL;
}

//To remove a binding from the symbol table
void *SymTable_remove(SymTable_t oSymTable, const char *pcKey)
{
    char *str;
    struct Node *temp1 = NULL, *temp = oSymTable->firstnode;
    assert(pcKey != NULL);
    if (temp != NULL)
    {
        if (strcmp(temp->key, pcKey) == 0)
        {
            temp1 = temp;
            temp = temp->next;
            temp1->next = NULL;
            oSymTable->firstnode = temp;
            str = (char*)temp1->value;
            free((char*)temp1->key);
            free(temp1);
            return str;
        }
    }
    while (temp->next != NULL)
    {
        if (strcmp(temp->next->key, pcKey) == 0)
        {
            temp1 = temp->next;
            temp->next = temp->next->next;
            temp1->next = NULL;
            str = (char*)temp1->value;
            free((char*)temp1->key);
            free(temp1);
            return str;
        }
        temp = temp->next;
    }
    return NULL;
}

//To replace a binding from the symbol table
void *SymTable_replace(SymTable_t oSymTable, const char *pcKey, const void *pvValue)
{
    char *str;
    struct Node *n, *temp1 = NULL, *temp = oSymTable->firstnode;
    assert(pcKey != NULL);
    assert(pvValue != NULL);
    n = malloc(sizeof(struct Node));
    while (temp != NULL)
    {
        if (strcmp(temp->key, pcKey) == 0)
        {
            n->key = malloc(sizeof(char) * strlen(pcKey) + 1);
            strcpy((char*)n->key, temp->key);
            n->value = temp->value;
            temp1 = n;
            temp->value = pvValue;
            str = (char*)n->value;
            free((char*)n->key);
            free(n);
            return str;
        }
        temp = temp->next;
    }
    free(n);
    return NULL;
}

//To implement MAP using a function pointer
void SymTable_map(SymTable_t oSymTable, void(*pfApply) (const char *pcKey, const void *pvValue, void *pvExtra), const void *pvExtra)
{
    if (oSymTable == NULL)
        return;
    printf("\nMap Test - Printing Entries.\n");
    struct Node *temp = oSymTable->firstnode;
    while (temp != NULL)
    {
        (*pfApply)(temp->key, temp->value, (char*)pvExtra); //calls printBinding function
        temp = temp->next;
    }
}
